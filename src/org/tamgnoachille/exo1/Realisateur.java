package org.tamgnoachille.exo1;

public class Realisateur {

	/*champ*/
	private String nom;
	private String prenom;

	/*constructor*/
	public Realisateur(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	/* getters and setters */
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}






}
