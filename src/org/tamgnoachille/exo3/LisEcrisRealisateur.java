package org.tamgnoachille.exo3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.tamgnoachille.exo2.Film;
import org.tamgnoachille.exo2.Realisateur;

import org.tamgnoachille.exo2.*;

public class LisEcrisRealisateur {

	private Writer writer;

	public LisEcrisRealisateur(Writer writer){
		this.writer = writer;
	}

	public void sauveRealisateur(Realisateur realisateur){

		try {

			PrintWriter pw =new PrintWriter(writer);

			pw.append(realisateur.toString());
			pw.append("\n");

		}
		finally{
			if(writer != null){
				try{
					writer.close();
				} catch(IOException e){
					System.out.println("Erreur : " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}


	public Film readFilm(String line){
		Film film =null;
		StringTokenizer st = new StringTokenizer(line, ",");
		while(st.hasMoreTokens()){
			film = new Film(st.nextToken(),Integer.parseInt((st.nextToken().trim())));
		}
		return film;
	}


	public Realisateur readRealisateur(String fileName){
		File file = new File(fileName);
		Reader reader =null;
		try {

			BufferedReader br = new BufferedReader(reader = new FileReader(file));
			String line ,line2; 
			Film film=null;
			Realisateur realisateur = null;

			//
			// Premiere ligne pour lire le nom/prenom du realisateur
			//
			line = br.readLine();

			if (line  != null){
				//line = br.readLine();

				StringTokenizer st = new StringTokenizer(line, ",|:| ");

				while(st.hasMoreTokens()){
					st.nextToken();
					realisateur = new Realisateur(st.nextToken(),st.nextToken(),Type.REALISATEUR);

					//String line2 = br.readLine();
				}
			}

			//
			// A partir de la deuxi�me ligne, en lit les films (on suppose qu'on a qu'un seul realisateur dans le fichier
			// sinon on peut l'adapter pour lire tous les realisateurs dans le fichier
			//

			while ((line2 = br.readLine()) !=null){
				StringTokenizer st2 = new StringTokenizer(line2, ",");

				while(st2.hasMoreTokens()){
					film = this.readFilm(line2);
					realisateur.addFilm(film);
				}
			}

			br.close();
			return realisateur;

		}  catch (IOException e) {
			System.out.println("Erreur : " + e.getMessage());
			e.printStackTrace();
		}
		finally{
			try {
				reader.close();
			} catch (IOException e) {
				System.out.println("Erreur : " + e.getMessage());
				e.printStackTrace();
			}
		}
		return null;
	}
}

