package org.tamgnoachille.exo3;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import org.tamgnoachille.exo2.*;


public class Main {

	public static void main(String[] args) throws IOException {
		Realisateur realisateur1 = new Realisateur("Lautner", "Georges", null);
		//		Realisateur realisateur2 = new Realisateur("Ouhami", "Jawad", Type.REALISATEUR);
		//		Realisateur realisateur3 = new Realisateur("Michael", "David", Type.REALISATEUR);

		Film film1 = new Film("Les Tontons Flingtueurs", 1963);
		Film film2 = new Film("Les Barbouzes", 1964);
		Film film3 = new Film("Nous ne achons pas", 1966);
		Film film4 = new Film("Examen de Java", 2015);


		//
		// Ajour des films a la liste des films realise par le realisateur1
		//
		realisateur1.addFilm(film1);
		realisateur1.addFilm(film2);
		realisateur1.addFilm(film3);
		realisateur1.addFilm(film4);

		//		System.out.println(realisateur1);

		//
		// Suppression du film4
		//
		realisateur1.removeFilm(film4);

		//		System.out.println(realisateur1);
		//		
		//		System.out.println("Le film realise par " + realisateur1.getNom() + " " + realisateur1.getPrenom() + " en 1963 : " + realisateur1.getFilm(1963));

		Writer writer = new FileWriter(new File("realisateurs.txt"));

		LisEcrisRealisateur ler = new LisEcrisRealisateur(writer);

		ler.sauveRealisateur(realisateur1);


		System.out.println("Test de lecture d'un film sur une ligne => " + ler.readFilm("Les Tontons Flingtueurs, 1963"));

		System.out.println(ler.readRealisateur("realisateurs.txt"));



	}

}
