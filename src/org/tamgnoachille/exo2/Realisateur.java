package org.tamgnoachille.exo2;

import java.security.KeyStore.Entry;
import java.util.*;


public class Realisateur extends Personne{
	private ArrayList<Film> list = new ArrayList<Film>();
    
	private Type type = Type.REALISATEUR;
	
	/*champ*/
	private String nom;
	private String prenom;
	private Film film;

	/*constructor*/

	public Realisateur(String nom, String prenom, Type realisateur) {
		super(nom, prenom);
		
	}

	/**** Method addFilm ****/
	/*public boolean addMarin(Film film){
		if (!list.contains(film)){
			list.add(film);
			return true;
		}
		return false; 
	}

	/**** Method removeFilm **/
	/*public boolean removeFilm(Film film){
		return this.list.remove(film);
	}


	/** getNombreFilm() **/
	/*public int getNombreFilm(){
		return  list.size();
	}*/


	private TreeMap<Integer, Film> filmMap = new TreeMap<Integer,Film>();

	// retourne le filme ajout�
	public Film addFilm(Film film){
		return this.filmMap.put(film.getAnneeRealisation(), film);
	}

	//	retourne le filme supprim�
	public Film removeFilm(Film film){
		return this.filmMap.remove(film.getAnneeRealisation ());
	}

	public int getNombreFilm(){
		return this.filmMap.size();
	}

	public Film getFilm(int annee){
		return this.filmMap.get(annee);
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(type).append(" : ").append(this.getNom()).append(" ").
		append(this.getPrenom()).append("\n");
		for (java.util.Map.Entry<Integer, Film> entry : this.filmMap.entrySet() ){
			sb.append(entry.getValue().getTitre()).append(", ").
			append(entry.getValue().getAnneeRealisation()).append("\n");
		}
		return sb.toString();
	}



	/*toString method*/
	/*public String toString() {
		return "Realisateur [nom=" + nom + ", prenom=" + prenom +",\n"
				+film.getTitre() + +film.getAnneeRealisation()+",\n"+ "]";
	}*/







}
