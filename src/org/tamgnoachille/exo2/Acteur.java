package org.tamgnoachille.exo2;

public class Acteur extends Personne  {

	/*champs*/
	private String nom;
	private String prenom;

	/*constructor*/
	public Acteur(String nom, String prenom) {
		super(nom, prenom);
	}

	/*toString method*/
	public String toString() {
		return "Acteur [nom=" + nom + ", prenom=" + prenom + "]";
	}

}
