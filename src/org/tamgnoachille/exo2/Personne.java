package org.tamgnoachille.exo2;

public abstract class Personne {
	
	/*champs*/
	private String nom;
	private String prenom;
	
	/*constructor*/
	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}
	
	/* getters and setters */
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	/* hashCode method*/
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

    /*equals method */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acteur other = (Acteur) obj;
		if (nom == null) {
			if (other.getNom() != null)
				return false;
		} else if (!nom.equals(other.getNom()))
			return false;
		if (prenom == null) {
			if (other.getPrenom() != null)
				return false;
		} else if (!prenom.equals(other.getPrenom()))
			return false;
		return true;
	}
	
	
	

}
