package org.tamgnoachille.exo2;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;



public class Main {

	public static void main(String[] args) {


		SortedSet<Acteur> set = new TreeSet<Acteur>(new Comparator<Acteur>() {

			public int compare(Acteur m1, Acteur m2 ){
				if (m1.getNom().equals(m2.getNom())){
					return m1.getPrenom().compareTo(m2.getPrenom());
				}else{
					return m1.getNom().compareTo(m2.getNom());
				}
			}
		}); 

		Acteur m1 = new Acteur("Tabarly", "Eric");
		Acteur m2 = new Acteur("Tabarly", "Eric"); // doublon
		Acteur m3 = new Acteur("Auguin", "Alain");

		set.add(m1); 
		set.add(m2); 
		set.add(m3);

		// verification du contenu

		for (Acteur m : set){
			System.out.println(m);
		}
		
		
		Realisateur realisateur1 = new Realisateur("Michael", "David", null);
		
		
		Film film1 = new Film("Les Tontons Flingtueurs", 1963);
		
		
		//
		// Ajour des films a la liste des films realise par le realisateur1
		//
		realisateur1.addFilm(film1);
	}
}
